/* eslint-disable no-console */
const express = require('express');

const path = require('path');
const fs = require('fs');

const app = express();
const morgan = require('morgan');

const accessLogStream = fs.createWriteStream(path.join('middleware', 'access.log'), { flags: 'a' });
app.use(morgan('combined', { stream: accessLogStream }));
const directorrouter = require('./routes/director');
const movierouter = require('./routes/movies');

app.use('/api/directors', directorrouter);
app.use('/api/movies', movierouter);


const port = process.env.PORT || 5000;
app.listen(port, console.log(`listening to the port ${port}`));
