/* eslint-disable prefer-promise-reject-errors */
const mysql = require('mysql');

const createConnection = require('../utils/connection');

const connection = createConnection(mysql, '172.17.0.2', 'root', 'root', 'movies');
connection.connect();

function getAllTheMovies() {
  return new Promise((resolve, reject) => {
    connection.query('SELECT * FROM movies', (err, result) => {
      if (err) {
        console.log('There is problem in the request');
        reject(err);
      }
      resolve(result);
    });
  });
}

function getMovieWithGivenId(movieId) {
  return new Promise((resolve, reject) => {
    connection.query(
      'SELECT * FROM movies where  moviesId=?',
      movieId,
      (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }
        resolve(result[0]);
      },
    );
  });
}

function addMovie(object) {
  return new Promise((resolve, reject) => {
    connection.query('INSERT into movies SET ?', object, (err, result) => {
      if (err) {
        reject('The directorId is not present in the directors table');
      }
      resolve('new movie is added into the movie table');
    });
  });
}

function checkIfTheIdExists(id) {
  return new Promise((resolve, reject) => {
    connection.query(
      'SELECT  moviesId from movies where  moviesId=?',
      id,
      (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }
        resolve(result[0]);
      },
    );
  });
}

function deleteTheMovie(id) {
  return new Promise(async (resolve, reject) => {
    const idStatus = await checkIfTheIdExists(id);
    if (idStatus !== undefined) {
      connection.query('DELETE from movies where  moviesId=?', id, (err) => {
        if (err) {
          console.log(err);
          reject(err);
        }
        resolve(`the movie with the id ${id} is deleted from the table`);
      });
    } else {
      resolve('the requested id is not present');
    }
  });
}

function updateTheMovie(id, object) {
  return new Promise(async (resolve, reject) => {
    const idStatus = await checkIfTheIdExists(id);
    if (idStatus !== undefined) {
      connection.query(
        'UPDATE movies SET ? where  moviesId=?',
        [object, id],
        (err) => {
          if (err) {
            console.log(err);
            reject(err);
          }
          resolve(`the movie with the id:${id} is updated`);
        },
      );
    } else {
      resolve('the requested id is not present');
    }
  });
}
//  async function check(){
//    var c = await addMovie({
//     "Rank": 10,
//     "Title": "Inception",
//     "Description": "A thief, who steals corporate secrets through the use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.",
//     "Runtime": 148,
//     "Genre": "Action",
//     "Rating": 8.8,
//     "Metascore": 74,
//     "Votes": 1692709,
//     "Gross_Earning_in_Mil": 292.58,
//     "Director_id": 3,
//     "Actor": "Leonardo DiCaprio",
//     "Year": 2010
//   });
//    console.log(c);
//  }
//  check()

module.exports = {
  updateTheMovie,
  deleteTheMovie,
  addMovie,
  getMovieWithGivenId,
  getAllTheMovies,
};

