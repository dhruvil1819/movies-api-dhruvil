/* eslint-disable no-unused-vars */
const mysql = require('mysql');

const createConnection = require('../utils/connection');

const connection = createConnection(mysql, '172.17.0.2', 'root', 'root', 'movies');

connection.connect();
function getAllDirectors() {
  return new Promise((resolve, reject) => {
    connection.query('SELECT Director FROM directors', (err, result) => {
      if (err) {
        console.log('There is some problem in the request');
        reject(err);
      }
      resolve(result);
    });
  });
}


function getDirectorWithGivenId(id) {
  return new Promise((resolve, reject) => {
    connection.query('SELECT Director FROM directors where id=?', id, (err, result) => {
      if (err) {
        console.log('the requested id is not present');
      }
      resolve(result);
    });
  });
}

function updateDirectorWithGivenId(id, directorname) {
  return new Promise((resolve, reject) => {
    connection.query('UPDATE directors SET Director=? where id=?', [directorname, id],
      (err, result) => {
        if (err) {
          reject(err);
        }
        resolve(`the update is succsessful with id${id}`);
      });
  });
}

function checkIfTheDirectorExists(directorname) {
  return new Promise((resolve, reject) => {
    connection.query('SELECT Director from directors where Director=?', directorname,
      (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }
        resolve(result[0]);
      });
  });
}

function checkIfTheIdExists(id) {
  return new Promise((resolve, reject) => {
    connection.query('SELECT id from directors where id=?', id,
      (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }
        resolve(result[0]);
      });
  });
}


function addTheDirector(directorname) {
  return new Promise(async (resolve, reject) => {
    const directorStatus = await checkIfTheDirectorExists(directorname);
    if (directorStatus === undefined) {
      const Director = { Director: directorname };
      connection.query('INSERT into directors SET ?', Director,
        (err, result) => {
          if (err) {
            console.log(err);
            reject(err);
          }

          resolve('The director has been added succcessfully');
        });
    } else {
      console.log('The director is already present please use update');
      resolve('The director is already present please use update');
    }
  });
}

function deleteTheDirector(id) {
  return new Promise(async (resolve, reject) => {
    const idStatus = await checkIfTheIdExists(id);
    if (idStatus !== undefined) {
      connection.query('DELETE from directors where id=?', id, (err) => {
        if (err) {
          console.log(err);
          reject(err);
        }
        resolve(`the director with the id ${id} is deleted from the table`);
      });
    } else {
      resolve('the requested id is not present');
    }
  });
}

module.exports = {
  deleteTheDirector,
  addTheDirector,
  updateDirectorWithGivenId,
  getDirectorWithGivenId,
  getAllDirectors,
};
