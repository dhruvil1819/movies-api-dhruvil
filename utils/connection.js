

function connection(mysql, host, user, password, database) {
  return mysql.createConnection({
    host: `${host}`,
    user: `${user}`,
    password: `${password}`,
    database: `${database}`,

  });
}
module.exports = connection;
