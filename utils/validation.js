const joi = require('@hapi/joi');

const schema = joi.object().keys({
  a: joi.string(),
});


const idSchema = joi.object().keys({
  id: joi.number().integer(),
});

const directorSchema = joi.object().keys({
  directorName: joi.string(),
});

const movieSchema = joi.object().keys({
  Rank: joi.number().integer(),
  Title: joi.string(),
  Description: joi.string(),
  Runtime: joi.number(),
  Genre: joi.string(),
  Rating: joi.number(),
  Metascore: joi.number(),
  Votes: joi.number(),
  Gross_Earning_in_Mil: joi.number(),
  Director_id: joi.number().integer(),
  Actor: joi.string(),
  Year: joi.number().integer(),
});
module.exports = {
  idSchema,
  directorSchema,
  movieSchema,
};
