/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
/* eslint-disable global-require */
const mysql = require('mysql');
const createConnection = require('../utils/connection');

const connection = createConnection(mysql, '172.17.0.2', 'root', 'root', 'movies');


connection.connect();
function createTable(name, schema) {
  return new Promise((resolve, rejected) => {
    connection.query(`CREATE TABLE  ${name} 
    ${schema}`, (error, results, fields) => {
      if (error) {
        rejected(error);
      }
      resolve();
    });
  });
}
function droptable(name) {
  return new Promise((resolve, rejected) => {
    connection.query(`DROP TABLE IF EXISTS ${name}`, (error, results, fields) => {
      if (error) {
        rejected(error);
      }
      resolve();
    });
  });
}


function createDirectorTable(jsonData) {
  return new Promise((resolve, reject) => {
    const uniqueSet = new Set();
    jsonData.forEach((element) => {
      if (!uniqueSet.has(element.Director)) {
        uniqueSet.add(element.Director);
        const directorName = { Director: element.Director };
        connection.query('INSERT INTO directors SET ?', directorName, (error, results, fields) => {
          if (error) reject(error);
          resolve();
        });
      }
    });
  });
}
function getTheId(Director) {
  return new Promise((resolve, reject) => {
    connection.query('SELECT id from directors where Director = ?', Director, (error, results, fields) => {
      if (error) reject(error);
      resolve(results[0].id);
    });
  });
}
function createMovieTable(jsonData) {
  return new Promise((resolve, reject) => {
    jsonData.forEach(async (element) => {
      const id = await getTheId(element.Director);
      delete element.Director;
      element.Director_id = id;
      connection.query('INSERT INTO movies SET ?', element, (error, results, fields) => {
        if (error) throw error;
        resolve();
      });
    });
  });
}
async function populateTable() {
  const array = [];
  //   array.push(droptable('movies'));
  array.push(droptable('movies'));
  array.push(droptable('directors'));
  array.push(createTable('directors', '(  id integer  PRIMARY KEY  AUTO_INCREMENT,Director text)'));
  array.push(createTable('movies', `(
       moviesId INTEGER PRIMARY KEY  AUTO_INCREMENT,
       Rank INTEGER ,
       Title TEXT,
       Description TEXT,
       Runtime INTEGER,
       Genre  TEXT,
       Rating FLOAT,
       Metascore TEXT,
       Votes MEDIUMINT,
       Gross_Earning_in_Mil TEXT,
       Director_id INTEGER,
       FOREIGN KEY (Director_id) REFERENCES directors(id) ON DELETE CASCADE ON UPDATE CASCADE,
       Actor TEXT,
       Year INTEGER
         )`));
  const jsonData = require('../data/data.json');
  array.push(createDirectorTable(jsonData));
  array.push(createMovieTable(jsonData));
  await Promise.all(array);
  connection.end();
}

populateTable();
