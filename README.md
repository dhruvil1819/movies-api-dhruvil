Creating a restful-api for top 50 movies based on the imdb rating

This are the following task that has been done in the project
1. Creating a get api to retrive any movie information based on the id provided 
2. Creating a post api to add new movie to the list
3. Creating a put api to edit the any movie in the list based on the id
4. Creating a delete api to delete any movie from the list based on the id

Technologies used:
Node.js,Express,MySql
