const express = require('express');

const joi = require('@hapi/joi');
const schema = require('../utils/validation');


const { idSchema } = schema;
const { movieSchema } = schema;
const router = express.Router();
const functions = require('../models/movies');
const bodyparser = require('body-parser');

router.use(bodyparser.json());

router.get('/movies', async (req, res) => {
  const { getAllTheMovies } = functions;
  const result = await getAllTheMovies();
  res.send(result);
});


router.get('/:movieId', (req, res) => {
  const { getMovieWithGivenId } = functions;
  joi.validate({ id: req.params.movieId }, idSchema, async (error) => {
    if (error) {
      res.send('the request id should be integer');
    } else {
      const result = await getMovieWithGivenId(req.params.movieId);
      res.send(result);
    }
  });
});

router.put('/:movieId', (req, res) => {
  const { updateTheMovie } = functions;
  joi.validate({ id: req.params.movieId }, idSchema, (error) => {
    if (error) {
      res.send('the request id should be integer');
    } else {
      joi.validate(req.body, movieSchema, async (error) => {
        if (error) {
          res.send('the schema validation failed');
        } else {
          const result = await updateTheMovie(req.params.movieId, req.body);
          res.send(result);
        }
      });
    }
  });
});

router.post('/', (req, res) => {
  const { addMovie } = functions;
  joi.validate(req.body, movieSchema, async (error) => {
    if (error) {
      res.send('the schema validation failed');
    } else {
      const result = await addMovie(req.body);
      res.send(result);
    }
  });
});

router.delete('/:movieId', (req, res) => {
  const { deleteTheMovie } = functions;
  joi.validate({ id: req.params.movieId }, idSchema, async (error) => {
    if (error) {
      res.send('the requested id should be the integer');
    } else {
      const result = await deleteTheMovie(req.params.movieId);
      res.send(result);
    }
  });
});



// export this router to use in our index.js
module.exports = router;
