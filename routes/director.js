const express = require('express');

const joi = require('@hapi/joi');
const schema = require('../utils/validation');

const { idSchema } = schema;
const { directorSchema } = schema;


const router = express.Router();
const functions = require('../models/director');
const bodyparser = require('body-parser');

router.use(bodyparser.text());


router.get('/directors', async (req, res) => {
  const { getAllDirectors } = functions;
  const result = await getAllDirectors();
  res.send(result);
});


router.get('/:directorId', (req, res) => {
  const { getDirectorWithGivenId } = functions;
  joi.validate({ id: req.params.directorId }, idSchema, async (error) => {
    if (error) {
      res.send('the requested id should be the integer');
    } else {
      const result = await getDirectorWithGivenId(req.params.directorId);
      res.send(result);
    }
  });
});

router.put('/:directorId', async (req, res) => {
  const { updateDirectorWithGivenId } = functions;
  joi.validate({ id: req.params.directorId }, idSchema, async (error) => {
    if (error) {
      res.send('the requested id should be the integer');
    } else {
      joi.validate({ directorName: req.body }, directorSchema, async (error) => {
        if (error) {
          res.send('schema validation failed');
        } else {
          const result = await updateDirectorWithGivenId(req.params.directorId, req.body);
          res.send(result);
        }
      });
    }
  });
});

router.post('/', async (req, res) => {
  const { addTheDirector } = functions;
  joi.validate({ directorName: req.body }, directorSchema, async (error) => {
    if (error) {
      res.send('schema validation failed');
    } else {
      const result = await addTheDirector(req.body);
      res.send(result);
    }
  });
});

router.delete('/:directorId', (req, res) => {
  const { deleteTheDirector } = functions;
  joi.validate({ id: req.params.directorId }, idSchema, async (error) => {
    if (error) {
      res.send('the requested id should be integer');
    } else {
      const result = await deleteTheDirector(req.params.directorId);
      res.send(result);
    }
  });
});

// export this router to use in our index.js
module.exports = router;
